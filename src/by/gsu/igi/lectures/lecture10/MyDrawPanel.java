package by.gsu.igi.lectures.lecture10;

import javax.swing.*;
import java.awt.*;

/**
 * Created: 21.11.2016
 *
 * @author Evgeniy Myslovets
 */
public class MyDrawPanel extends JPanel {

    @Override
    protected void paintComponent(Graphics g) {
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        int red = (int) (Math.random() * 255);
        int green = (int) (Math.random() * 255);
        int blue = (int) (Math.random() * 255);

        Color randomColor = new Color(red, green, blue);
        g.setColor(randomColor);
        g.fillOval(70, 70, 100, 100);
    }
}

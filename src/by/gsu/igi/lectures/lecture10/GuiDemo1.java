package by.gsu.igi.lectures.lecture10;

import javax.swing.*;

/**
 * Created: 21.11.2016
 *
 * @author Evgeniy Myslovets
 */
public class GuiDemo1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        JButton button = new JButton("Нажми на меня");

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.getContentPane().add(button);

        frame.setSize(300, 300);

        frame.setVisible(true);
    }
}

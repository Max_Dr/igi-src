package by.gsu.igi.lectures.lecture04;

public abstract class Animal implements Touchable {
    private int age;

    public int getAge() {
        return age;
    }

    public void becomeOlder() {
        age++;
    }

    public abstract void makeSound();
}

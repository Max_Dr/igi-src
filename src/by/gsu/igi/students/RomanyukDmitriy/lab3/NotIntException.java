package by.gsu.igi.students.RomanyukDmitriy.lab3;

/**
 * Created by Romanyuk Dmitriy.
 */
public class NotIntException extends RuntimeException {

    public NotIntException(String str) {
        super(str + " not int! Error");
    }
}

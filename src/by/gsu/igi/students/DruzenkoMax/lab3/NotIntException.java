package by.gsu.igi.students.DruzenkoMax.lab3;

/**
 * Created by Druzenko Max.
 */
public class NotIntException extends RuntimeException {

    public NotIntException(String str) {
        super(str + " not int! Error");
    }
}

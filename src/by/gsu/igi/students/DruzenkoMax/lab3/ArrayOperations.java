package by.gsu.igi.students.DruzenkoMax.lab3;

import java.util.Scanner;
import java.lang.Math;

/**
 * Created by Druzenko Max.
 */
public class ArrayOperations {

    public static int findMax(int[] numbers) {
        int max = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if (max < numbers[i]) {
                max = numbers[i];
            }
        }

        return max;
    }

    public static int findMin(int[] numbers) {
        int min = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if (min > numbers[i]) {
                min = numbers[i];
            }
        }

        return min;
    }

    public static double calculateAverage(int[] numbers) {
        int average = 0;

        for (int number : numbers) {
            average += number;
        }

        return average / (double) numbers.length;
    }

    public static int product(int[] numbers) {
        int product = 1;

        for (int i = 0; i < numbers.length; i++) {
            product *= numbers[i];
        }

        return product;
    }

    public static int sum(int[] numbers) {
        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }

        return sum;
    }

    public static int difference(int[] numbers) {
        return -sum(numbers);
    }

    public static void main(String[] args) {
        int[] numbers = createArray(args);

        // вызываем вычислительные методы для полученного массива
        processArray(numbers);
    }

    private static int[] createArray(String[] str) {
        while (true) {
            System.out.println("Как вы хотите ввести данные? 1 - консоль, 2 - ком. строка, 3 - рандом");

            Scanner scanner = new Scanner(System.in);
            int number = checkInt(scanner.next());
            switch (number) {
                case 1: {
                    return readArray();
                }

                case 2: {
                    if (str.length != 0) {
                        return readArray(str);
                    } else {
                        System.out.println("В командной строке нет данных, выберите либо 1, либо 3");
                    }
                    break;
                }

                case 3: {
                    return readArrayRandom();
                }

                default: {
                    System.out.println("Вводить нужно только 1, либо 2, либо 3!");
                }
            }
        }
    }

    private static int[] readArray() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество элементов:");
        int size = checkInt(scanner.next());

        int[] numbers = new int[size];

        for (int i = 0; i < size; i++) {
            System.out.println("Введите " + (i + 1) + "-й элемент:");
            numbers[i] = checkInt(scanner.next());
        }
        return numbers;
    }

    private static int[] readArray(String[] str) {
        int size = checkInt(str[0]);

        System.out.println(size);

        int[] numbers = new int[size];

        for (int i = 1; i <= size; i++) {
            numbers[i - 1] = checkInt(str[i]);
            System.out.println(str[i]);
        }

        return numbers;
    }

    private static int[] readArrayRandom() {
        int size = (int) (Math.random() * 10) + 1;

        System.out.println(size);

        int[] numbers = new int[size];

        for (int i = 0; i < size; i++) {
            numbers[i] = (int) (Math.random() * 100);
            System.out.println(numbers[i]);
        }

        return numbers;
    }

    private static int checkInt(String str) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            throw new NotIntException(str);
        }
    }

    private static void processArray(int[] numbers) {
        int max = findMax(numbers);
        System.out.println("Максимальный элемент: " + max);

        int min = findMin(numbers);
        System.out.println("Минимальный элемент: " + min);

        double average = calculateAverage(numbers);
        System.out.println("Среднее значение: " + average);

        int product = product(numbers);
        System.out.println("Произведение элементов: " + product);

        int sum = sum(numbers);
        System.out.println("Сумма элементов: " + sum);

        int difference = difference(numbers);
        System.out.println("Разность элементов: " + difference);
    }
}

package by.gsu.igi.students.AlinaGonchar.lab3;

/**
 * Created by AlinaGonchar
 */
public class NotIntException extends RuntimeException {

    public NotIntException(String str) {
        super(str + " not int! Error");
    }
}
